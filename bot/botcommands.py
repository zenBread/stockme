import aiohttp
from typing import List, Optional, Dict, Tuple
from bs4 import BeautifulSoup
import decimal


async def sYahoo(symbols: List[str]):
    """ Scrape Yahoo for symbols """
    stocks = {}
    async with aiohttp.ClientSession() as session:

        for symbol in symbols:
            url = f'https://finance.yahoo.com/quote/{symbol}'
            async with session.get(url) as resp:
                soup = BeautifulSoup(await resp.read(), 'html.parser')

                alldata = soup.find_all("tbody")
                try:
                    table1 = alldata[0].find_all("tr")
                    table1 += alldata[1].find_all("tr")
                except:
                    table1=None

                stocks[symbol] = {}
                try:
                    currentSpan = soup.find_all("span", {"data-reactid": "50"})
                    stocks[symbol]["current"] = currentSpan[0].text
                except IndexError:
                    currentSpan = soup.find_all("span", {"data-reactid": "33"})
                    stocks[symbol]["current"] = currentSpan[1].text
                for i in range(0,len(table1)):
                    try:
                        table1_td = table1[i].find_all("td")
                    except:
                        table1_td = None
                    if table1_td[1].text == 'N/A':
                        continue
                    stocks[symbol][table1_td[0].text] = table1_td[1].text
    return stocks


async def getUserStocks(user: str):
    async with aiohttp.ClientSession() as session:
        url = f'http://localhost:8000/stocks/{user}'
        async with session.get(url) as resp:
            return await resp.json()


async def getStats(symbol: str):
    async with aiohttp.ClientSession() as session:
        url = f'http://localhost:8000/stats/{symbol}'
        async with session.get(url) as resp:
            return await resp.json()


async def postStock(user: str, *stocks: Tuple[Dict[str, Dict[str, str]]]):
    payload = []
    for name, stock in stocks[0].items():
        low, _, high = stock.get("Day's Range", '0').partition(' - ')
        current = float(stock.get('current', '0'))
        volume = float(stock.get('Volume', '0').replace(',', '_'))
        peRatio = float(stock.get('PE Ratio (TTM)', '0'))
        beta = float(stock.get('Beta (5Y Monthly)', '0'))
        close = float(stock.get('Previous Close', '0'))
        high = float(high)
        low = float(low)
        payload.append({
            "id": 0, 
            "symbol": name,
            "current": current,
            "volume": volume,
            "peRatio": peRatio,
            "beta": beta,
            "close": close,
            "high": high,
            "low": low
            })
    async with aiohttp.ClientSession() as session:
        url = f'http://localhost:8000/stocks/{user}'
        async with session.post(url, json=payload) as resp:
            return await resp.json()


async def postUser(user: str, *stocks: Tuple[Dict[str, Dict[str, str]]]):
    newUser = {"id": 0,
               "name": user,
               }
    newStocks = []
    for name, stock in stocks[0].items():
        low, _, high = stock.get("Day's Range", '0').partition(' - ')
        current = float(stock.get('current', '0'))
        volume = float(stock.get('Volume', '0').replace(',', '_'))
        peRatio = float(stock.get('PE Ratio (TTM)', '0'))
        beta = float(stock.get('Beta (5Y Monthly)', '0'))
        close = float(stock.get('Previous Close', '0'))
        high = float(high)
        low = float(low)
        newStocks.append({
            "id": 0, 
            "symbol": name,
            "current": current,
            "volume": volume,
            "peRatio": peRatio,
            "beta": beta,
            "close": close,
            "high": high,
            "low": low
            })
    payload = {}
    payload['user'] = newUser
    payload['stocks'] = newStocks
    async with aiohttp.ClientSession() as session:
        url = f'http://localhost:8000/users/{user}'
        async with session.post(url, json=payload) as resp:
           return await resp.json()