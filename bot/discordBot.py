from typing import Any, Dict
import discord
from discord.ext import commands
import botcommands
import os

bot = commands.Bot(command_prefix='!')


@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))


# @bot.event
# async def on_command_error(ctx: commands.Context, error: Any):
#     if isinstance(error, commands.CommandError):
#         await ctx.send("Bad syntax")
#         await ctx.message.delete()


# Get stats for users
@bot.group()
async def stats(ctx: commands.Context):
    if ctx.invoked_subcommand is None:
        await ctx.send("""Invalid stats command.\n \
                        !stats current @user\n \
                        !stats deep stock""")


@stats.command()
async def current(ctx: commands.Context, user: discord.User):
    name = user.name
    stocks = await botcommands.getUserStocks(name)
    for stock in stocks:
        await ctx.send(embed=stockEmbed(stock))


@stats.command()
async def deep(ctx: commands.Context, stock: str):
    stats = await botcommands.getStats(stock)
    embed = stockEmbed(stats)
    await ctx.send(embed=embed)


# Update user information (add: stock, user [stock])
@bot.group()
async def add(ctx: commands.Context):
    if ctx.invoked_subcommand is None:
        await ctx.send("Invalid add command....")


@add.command()
async def stock(ctx: commands.Context, user: discord.User, *args):
    name = user.name
    webResp = await botcommands.sYahoo(args)
    resp = await botcommands.postStock(name, webResp)
    embed = stockUserEmbed(resp)
    await ctx.send(embed=embed)


@add.command()
async def user(ctx: commands.Context, user: discord.User, *args):
    name = user.name
    webResp = await botcommands.sYahoo(args)
    resp = await botcommands.postUser(name, webResp)
    embed = stockUserEmbed(resp)
    await ctx.send(embed=embed)


def stockUserEmbed(resp: Dict[str, str]):
    embed = discord.Embed(
        title = resp['name'],
        description = "Currently held stocks...",
        color = discord.Color.blue(),
    )
    for stock in resp['stocks']:
        embed.add_field(name=stock['symbol'], value=stock['current'])
    return embed


def stockEmbed(resp: Dict[str, str]):
    embed = discord.Embed(
        title = resp['symbol'],
        color = discord.Color.orange(),
    )
    for key, value in resp.items():
        if key in ['symbol', 'id']:
            continue
        embed.add_field(name=key, value=value)
    return embed


bot.run(os.getenv('TOKEN'))