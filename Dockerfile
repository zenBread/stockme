FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7
WORKDIR /api
COPY api/requirements.txt .
RUN pip install -r requirements.txt --no-cache-dir
RUN pip install fastapi uvicorn
EXPOSE 8000
COPY api/ .
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]