from fastapi import FastAPI
from routes import stocks, users, stats
from database import models
from database.dbcontrol import engine

models.Base.metadata.create_all(bind=engine)


app = FastAPI()
app.include_router(stocks.router)
app.include_router(users.router)
app.include_router(stats.router)
