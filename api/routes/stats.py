from fastapi import APIRouter, HTTPException, Depends
from sqlalchemy.orm import Session
import crud
import deps
import schemas

router = APIRouter(
    prefix="/stats",
    tags=["stats"],
    responses={404: {"description": "Not found"}},
)


@router.get("/{stock}", response_model=schemas.StockStats)
async def getStats(stock: str, db: Session = Depends(deps.getDB)):
    if not crud.inTables(db, stock):
        raise HTTPException(status_code=404, detail="Stock table not found")
    return crud.getStats(db, stock)