from fastapi import APIRouter, HTTPException, Depends
from fastapi.responses import ORJSONResponse
from sqlalchemy.orm import Session
from typing import List, Optional
import crud
import deps
import schemas

router = APIRouter(
    prefix="/users",
    tags=["users"],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_model=List[schemas.User])
async def getUsers(db: Session=Depends(deps.getDB)):
    """Get all Users in database"""
    return crud.getUsersAll(db)


@router.post("/{username}", response_model=schemas.User)
async def addUser(user: schemas.User, 
                  stocks: Optional[List[schemas.StockVerbose]] = None,
                  db: Session=Depends(deps.getDB)
                  ):
    """Add user with optional stock list for user"""
    return crud.createUser(db, user, stocks)


@router.delete("/{username}", response_class=ORJSONResponse)
async def removeUser(username: str, db: Session=Depends(deps.getDB)):
    """Delete user from database"""
    user = crud.getUserByName(db, username)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    crud.deleteUser(db, user)
    return [{"message":"ok"}]