from fastapi import APIRouter, HTTPException, Depends
from typing import List
from fastapi.responses import ORJSONResponse
from database.models import Stock
from sqlalchemy.orm import Session
import crud
import deps
import schemas

router = APIRouter(
    prefix="/stocks",
    tags=["stocks"],
    responses={404: {"description": "Not found"}},
)


@router.get("/", response_model=List[schemas.StockLite])
async def getStocks(db: Session = Depends(deps.getDB)):
    return crud.getStocks(db)


@router.get("/{username}", response_model=List[schemas.StockVerbose])
async def getStocksName(username: str, db: Session = Depends(deps.getDB)):
    user = crud.getUserByName(db, username)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    
    return user.stocks


@router.post("/{username}", response_model=schemas.User)
async def addStock(username: str, stocks: List[schemas.StockVerbose], db: Session = Depends(deps.getDB)):
    """Add stocks to a user in the database"""
    user = crud.getUserByName(db, username)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return crud.addStocksToUser(db, user, stocks)


@router.delete("/{username}", response_model=List[schemas.StockLite])
async def removeStockFromUser(username: str, stocks: List[schemas.Stock], db: Session = Depends(deps.getDB)):
    """Remove stocks from user"""
    user = crud.getUserByName(db, username)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return crud.removeStockFromUser(db, user, stocks)


@router.delete("/remove/{stock}", response_class=ORJSONResponse)
async def removeStock(stock: str, db: Session = Depends(deps.getDB)):
    dbstock = crud.getStockBySymbol(db, stock)
    if not dbstock:
        raise HTTPException(status_code=404, detail="Stock not found")
    crud.deleteStock(db, dbstock)
    return [{"message":"ok"}]