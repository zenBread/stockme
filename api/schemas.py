from typing import List
from pydantic import BaseModel
from decimal import Decimal


class Stock(BaseModel):
    id: int
    symbol: str

    class Config:
        orm_mode = True


class StockVerbose(Stock):
    current: Decimal
    volume: Decimal
    peRatio: Decimal
    beta: Decimal
    close: Decimal
    high: Decimal
    low: Decimal


class StockStats(StockVerbose):
    sma10: Decimal
    ema15: Decimal
    # ema50: Decimal
    # ema100: Decimal


class StockLite(Stock):
    current: Decimal

    class Config:
        orm_mode = True


class User(BaseModel):
    id: int
    name: str
    stocks: List[StockVerbose] = []

    class Config:
        orm_mode = True
