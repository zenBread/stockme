from database.models import User, Stock
from sqlalchemy.orm import Session
from database.dbcontrol import engine
from sqlalchemy import inspect
import schemas
from decimal import Decimal

inspector = inspect(engine)
# User:

def getUsersAll(db: Session):
    return db.query(User).all()


def createUser(db: Session, user: schemas.User, stocks: schemas.StockVerbose):
    dbUser = User(name=user.name)
    db.add(dbUser)
    addStocksToUser(db, dbUser, stocks)
    return dbUser


def deleteUser(db: Session, user: User):
    db.delete(user)
    db.commit()


def getUserByName(db: Session, username:str):
    return db.query(User).filter(User.name == username).first()


# Stock:

def getStocks(db: Session):
    return db.query(Stock).all()


def getStockBySymbol(db: Session, symbol: str):
    return db.query(Stock).filter(Stock.symbol == symbol).first()


def addStocksToUser(db: Session, user: User, stocks: schemas.StockVerbose):
    for stock in stocks:
        dbstock = db.query(Stock).filter(Stock.symbol == stock.symbol).first()
        if not dbstock:
            stock = Stock(
                symbol=stock.symbol,
                current = stock.current,
                volume = stock.volume,
                peRatio = stock.peRatio,
                beta = stock.beta,
                close = stock.close,
                high = stock.high,
                low = stock.low,
                )
            db.add(stock)
        user.stocks.append(stock if not dbstock else dbstock)
    db.commit()
    return user


def removeStockFromUser(db: Session, user: User, stocks: schemas.Stock):
    for stock in stocks:
        for userStock in user.stocks:
            if stock.symbol == userStock.symbol:
                user.stocks.remove(userStock)
    db.commit()
    return user.stocks


def deleteStock(db: Session, stock: Stock):
    db.delete(stock)
    db.commit()


# Stats:
def getStats(db: Session, symbol: str):    
    stock = getStockBySymbol(db, symbol)
    stock.sma10 = getSMA10(db, symbol)
    stock.ema15 = getEMA15(db, symbol)
    return stock


# Helpers
def getSMA10(db: Session, symbol: str):
    smt = f"select avg(close) as avg from (select close from {symbol} \
            order by date desc \
            FETCH first 10 rows only) as foo;"
    cursor = db.execute(smt)
    return cursor.one()[0]


def getEMA15(db: Session, symbol: str):
    smt = f"select avg(close) as avg from (select close from {symbol} \
            order by date desc \
            OFFSET 1 \
            FETCH first 10 rows only) as foo;"
    cursor = db.execute(smt)
    sma = cursor.one()[0]
    smt = f"select close from {symbol} \
            order by date desc \
            FETCH first rows only;"
    cursor = db.execute(smt)
    close = cursor.one()[0]
    return (close - sma) * Decimal((2 / 16)) + sma


def inTables(db: Session, stock: str):
    if stock in inspector.get_table_names():
        return True
    return False