from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os


# SQLALCHEMY_DATABASE_URL = "postgresql://postgres:password@localhost/stockme"
engine = create_engine(os.getenv("DATABASE_URL"), echo=False)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)