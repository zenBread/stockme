from sqlalchemy import Column, ForeignKey, String, Integer, UniqueConstraint, Table, Numeric
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
User_Stock_Association = Table(
    'su_association',
    Base.metadata,
    Column('users_id', Integer, ForeignKey('users.id')),
    Column('stock_id', Integer, ForeignKey('stocks.id'))
    )

class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    stocks = relationship("Stock", secondary=User_Stock_Association, backref="users")


class Stock(Base):
    __tablename__ = "stocks"

    id = Column(Integer, primary_key=True, autoincrement=True)
    symbol = Column(String, nullable=False)
    current = Column(Numeric)
    volume = Column(Numeric)
    peRatio = Column(Numeric)
    beta = Column(Numeric)
    close = Column(Numeric)
    high = Column(Numeric)
    low = Column(Numeric)
    UniqueConstraint("symbol")
